/**
 * Created by magnolia on 9/23/16.
 */

$(document).ready(function(){

    //Method to validate letters, numbers, whitespaces, color , semicolon adn question marks
    jQuery.validator.addMethod("caracteres", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ,.; ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras, numeros, comas, puntos y signos de interrogacion.');

    //Method to validate letters, numbers, whitespaces, color , semicolon adn question marks
    jQuery.validator.addMethod("caracteres_desc", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ,.;¿? ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras, numeros, comas, puntos y signos de interrogacion.');

    // To validate fields
    $("#questionForm").validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            $(element.parent("div").addClass("form-animate-error"));
            error.appendTo(element.parent("div"));
        },
        success: function(label) {
            $(label.parent("div").removeClass("form-animate-error"));
        },
        rules: {
            "question[question]": {
                required: true,
                rangelength: [2, 100],
                caracteres_desc: true
            },
            "question[answer][0][answer]": {
                required: true,
                rangelength: [2, 100],
                caracteres: true
            },
            "question[answer][1][answer]": {
                required: false,
                rangelength: [2, 100],
                caracteres: true
            },
            "question[answer][2][answer]": {
                required: false,
                rangelength: [2, 100],
                caracteres: true
            },
            "question[answer][3][answer]": {
                required:  false,
                rangelength: [2, 100],
                caracteres: true
            }
        },
        messages: {
            "question[question]": {
                required: "Por favor ingrese una pregunta.",
                rangelength: "Ingrese una pregunta mayor a 2 y menor a 100 caracteres."
            },
            "question[answer][0][answer]": {
                required: "Por favor ingrese una respuesta.",
                rangelength: "Ingrese una respuesta mayor a 2 y menor a 100 caracteres."
            },
            "question[answer][1][answer]": {
                required: "Por favor ingrese una respuesta.",
                rangelength: "Ingrese una respuesta mayor a 2 y menor a 100 caracteres."
            },
            "question[answer][2][answer]": {
                required: "Por favor ingrese una respuesta.",
                rangelength: "Ingrese una respuesta mayor a 2 y menor a 100 caracteres."
            },
            "question[answer][3][answer]": {
                required: "Por favor ingrese una respuesta.",
                rangelength: "Ingrese una respuesta mayor a 2 y menor a 100 caracteres."
            }
        }
    });

});
