$(document).ready(function(){

    //Method to validate letters, numbers and whitespaces
    jQuery.validator.addMethod("caracteres", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras y numeros.');

    //Method to validate letters, numbers, whitespaces, color and semicolon
    jQuery.validator.addMethod("caracteres_desc", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ,.; ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras, numeros, comas y puntos.');

    // To validate fields
    $("#signupForm").validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            $(element.parent("div").addClass("form-animate-error"));
            error.appendTo(element.parent("div"));
        },
        success: function(label) {
            $(label.parent("div").removeClass("form-animate-error"));
        },
        rules: {
            "vocabulary[name]": {
                required: true,
                rangelength: [2, 100],
                caracteres: true
            },
            "vocabulary[description]": {
                rangelength: [2, 200],
                caracteres_desc: true
            },
            "vocabulary[status]": {
                required: true,
                range: [0, 2]
            }
        },
        messages: {
            "vocabulary[name]": {
                required: "Por favor ingresa un nombre para tu Categoria",
                rangelength: "Ingresa un nombre para tu Categoria mayor a 2 y menor a 100 caracteres"
            },
            "vocabulary[description]": {
                rangelength: "Ingresa un nombre para tu Categoria mayor a 2 y menor a 200 caracteres"
            },
            "vocabulary[status]": {
                required: "Por favor ingresa un Status para tu Categoria",
                range: "Ingresa un Estado Valido, 0 = Inactivo, 1 = Activo, 2 = Borrado"
            }
        }
    });

});
