$(document).ready(function(){

    //Method to validate letters, numbers and whitespaces
    jQuery.validator.addMethod("caracteres", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras y numeros.');

    // To validate fields
    $("#signupForm").validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            $(element.parent("div").addClass("form-animate-error"));
            error.appendTo(element.parent("div"));
        },
        success: function(label) {
            $(label.parent("div").removeClass("form-animate-error"));
        },
        rules: {
            "user_interviewed[email]": {
                required: true,
                email: true
            },
            "user_interviewed[type]": {
                required: true,
                range: [0, 1],
            },
            "user_interviewed[status]": {
                required: true,
                range: [0, 2]
            }
        },
        messages: {
            "user_interviewed[email]": {
                required: "Por favor ingresa un correo para tu registro de Usuario",
                email: "Ingresa un correo valido"
            },
            "user_interviewed[type]": {
                required: "Por favor ingresa un valor para el tipo de Usuario",
                range: "Ingresa un Tipo Valido, 0 = Prospecto, 1 = Empleado"
            },
            "user_interviewed[status]": {
                required: "Por favor ingresa un Status para tu Usuario",
                range: "Ingresa un Estado Valido, 0 = Inactivo, 1 = Activo, 2 = Borrado"
            }
        }
    });

});