$(document).ready(function(){

    //Method to validate letters, numbers and whitespaces
    jQuery.validator.addMethod("caracteres", function(value, element) {
        return this.optional( element ) || /^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras y numeros.');

    //Method to validate phone numbers with "()" and "-"
    jQuery.validator.addMethod("telefonos", function(value, element) {
        return this.optional( element ) || /^[0-9\(\)\- ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando numeros.');

    // To validate fields
    $("#signupForm").validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            $(element.parent("div").addClass("form-animate-error"));
            error.appendTo(element.parent("div"));
        },
        success: function(label) {
            $(label.parent("div").removeClass("form-animate-error"));
        },
        rules: {
            "information[first_name]": {
                required: true,
                rangelength: [2, 50],
                caracteres: true
            },
            "information[last_name]": {
                required: true,
                rangelength: [2, 50],
                caracteres: true
            },
            "information[mobile_phone]": {
                required: true,
                rangelength: [7, 20],
                telefonos: true
            },
            "information[phone]": {
                rangelength: [7, 20],
                telefonos: true
            },
            "information[level_study]": {
                rangelength: [2, 50],
                caracteres: true
            },
            "information[living_in]": {
                rangelength: [2, 50],
                caracteres: true
            },
            "information[birthdate]": {
                rangelength: [10, 10],
                date: true
            },
            "information[civil_status]": {
                caracteres: true
            }
        },
        messages: {
            "information[first_name]": {
                required: "Este campo es requerido para crear tu informacion",
                rangelength: "Por favor ingresa cuando menos 2 caracteres y un maximo de 50 caracteres para tus nombres",
                caracteres: "Favor de ingresar caracteres validos"
            },
            "information[last_name]": {
                required: "Este campo es requerido para crear tu informacion",
                rangelength: "Por favor ingresa cuando menos 2 caracteres y un maximo de 50 caracteres para tus apellidos",
                caracteres: "Favor de ingresar caracteres validos"
            },
            "information[mobile_phone]": {
                required: "Este campo es requerido para crear tu informacion",
                rangelength: "Por favor ingresa cuando menos 7 caracteres y un maximo de 20 caracteres para tu celular",
                telefonos: "Favor de ingresar caracteres validos"
            },
            "information[phone]": {
                rangelength: "Por favor ingresa cuando menos 7 caracteres y un maximo de 20 caracteres para tu telefono",
                telefonos: "Favor de ingresar caracteres validos"
            },
            "information[level_study]": {
                rangelength: "Por favor ingresa cuando menos 2 caracteres y un maximo de 50 caracteres para tu nivel de estudios",
                caracteres: "Favor de ingresar caracteres validos"
            },
            "information[living_in]": {
                rangelength: "Por favor ingresa cuando menos 2 caracteres y un maximo de 50 caracteres para el lugar donde vives",
                caracteres: "Favor de ingresar caracteres validos"
            },
            "information[birthdate]": {
                rangelength: "Por favor ingresa 10 caracteres para tu fecha de nacimiento",
                date: "Ingresa una fecha valida"
            },
            "information[civil_status]": {
                caracteres: "Favor de ingresar caracteres validos"
            }
        }
    });

});