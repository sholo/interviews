$(document).ready(function(){

    //Method to validate letters, numbers and whitespaces
    jQuery.validator.addMethod("caracteres", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras y numeros.');

    //Method to validate letters, numbers, whitespaces, color and semicolon
    jQuery.validator.addMethod("caracteres_desc", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ,.; ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras, numeros, comas y puntos.');

    // To validate fields
    $("#signupForm").validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            $(element.parent("div").addClass("form-animate-error"));
            error.appendTo(element.parent("div"));
        },
        success: function(label) {
            $(label.parent("div").removeClass("form-animate-error"));
        },
        rules: {
            "questionnaire[name]": {
                required: true,
                rangelength: [3, 100],
                caracteres: true
            },
            "questionnaire[description]": {
                required: true,
                rangelength: [3, 100],
                caracteres: true
            }
        },
        messages: {
            "questionnaire[name]": {
                required: "Por favor ingresa un nombre para tu cuestionario",
                rangelength: "El nombre para tu cuestionario debe de ser mayor a tres caracteres y menor a 100."
            },
            "questionnaire[description]": {
                required: "Por favor ingresa una descripcion para tu cuestionario",
                ranqelength:"Ingrese una descripcion mayor a tres caracteres y menor a 200."
            }
        }
    });

});