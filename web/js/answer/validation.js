$(document).ready(function(){

    //Method to validate letters, numbers, whitespaces, color and semicolon
    jQuery.validator.addMethod("caracteres_desc", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ,.; ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras, numeros, comas y puntos.');

    // To validate fields
    $("#signupForm").validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            $(element.parent("div").addClass("form-animate-error"));
            error.appendTo(element.parent("div"));
        },
        success: function(label) {
            $(label.parent("div").removeClass("form-animate-error"));
        },
        rules: {
            "answer[answer]": {
                required: true,
                rangelength: [2, 65535],
                caracteres_desc: true
            },
            "answer[right]": {
                required: true,
                range: [0, 1]
            },
            "answer[status]": {
                required: true,
                range: [0, 2]
            }
        },
        messages: {
            "answer[answer]": {
                required: "Por favor ingresa una Respuesta",
                rangelength: "Ingresa un nombre para tu Respuesta mayor a 2 caracteres"
            },
            "answer[right]": {
                required: "Ingresa un valor de Incorrecto o Correcto a tu respuesta"
            },
            "answer[status]": {
                required: "Por favor ingresa un Status para tu Respuesta",
                range: "Ingresa un Estado Valido, 0 = Inactivo, 1 = Activo, 2 = Borrado"
            }
        }
    });

});
