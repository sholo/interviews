$(document).ready(function(){

    //Method to validate letters, numbers and whitespaces
    jQuery.validator.addMethod("caracteres", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras y numeros.');

    // To validate fields
    $("#signupForm").validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            $(element.parent("div").addClass("form-animate-error"));
            error.appendTo(element.parent("div"));
        },
        success: function(label) {
            $(label.parent("div").removeClass("form-animate-error"));
        },
        rules: {
            "term_data[name]": {
                required: true,
                rangelength: [3, 100],
                caracteres: true
            },
            "term_data[machine_name]": {
                rangelength: [2, 200],
                caracteres: true
            },
            "term_data[description]":{
                rangelength: [10, 200],
                caracteres: true
            },
            "term_data[status]": {
                required: true,
                range: [0, 2]
            }
        },
        messages: {
            "term_data[name]": {
                required: "Por favor ingresa un nombre para tu Termino",
                rangelength: "Ingresa un nombre para tu Categoria mayor a 3 y menor a 100 caracteres"
            },
            "term_data[machine_name]": {
                required: "Ingresa machine_name",
                ranqelength:"Ingres nombre machine_name"
            },
            "term_data[description]": {
                required: "Ingrese una descripcion",
                rangelength: "Ingresa una descripcion mayor a 10 y menor a 200 caracteres"
            },
            "term_data[status]": {
                required: "Por favor ingresa un Status para tu Termino",
                range: "Ingresa un Status Valido, 0 = Inactivo, 1 = Activo, 2 = Borrado"
            }
        }
    });

});