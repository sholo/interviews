$(document).ready(function(){

    //Method to validate letters, numbers and whitespaces
    jQuery.validator.addMethod("caracteres", function(value, element) {
        return this.optional( element ) || /^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ,.#; ]+$/.test( value );
    }, 'Por favor ingresa un valor, utilizando solo letras y numeros.');

    // To validate fields
    $("#testForm").validate({
        errorElement: "em",
        errorPlacement: function(error, element) {
            $(element.parent("div").addClass("form-animate-error"));
            error.appendTo(element.parent("div"));
        },
        success: function(label) {
            $(label.parent("div").removeClass("form-animate-error"));
        },
        rules: {
            "test[name]": {
                required: true,
                rangelength: [2, 100],
                caracteres: true
            },
            "test[description]": {
                required: true,
                caracteres: true
            },
            "test[test_date_expiry_from]": {
                rangelength: [10, 10],
                date: true
            },
            "test[test_date_expiry_to]": {
                rangelength: [10, 10],
                date: true
            },
            "test[user][]": {
                required: true
            }
        },
        messages: {
            "test[name]": {
                required: "Este campo es requerido para crear tu Test",
                rangelength: "Por favor ingresa cuando menos 2 caracteres y un maximo de 100 caracteres para tu Test",
                caracteres: "Favor de ingresar caracteres validos"
            },
            "test[description]": {
                required: "Este campo es requerido para crear tu Test",
                rangelength: "Por favor ingresa cuando menos 2 caracteres",
                caracteres: "Favor de ingresar caracteres validos"
            },
            "test[test_date_expiry_from]": {
                rangelength: "Por favor ingresa 10 caracteres para tu fecha de inicio",
                date: "Ingresa una fecha valida"
            },
            "test[test_date_expiry_to]": {
                rangelength: "Por favor ingresa 10 caracteres para tu fecha de expiracion",
                date: "Ingresa una fecha valida"
            },
            "test[user][]": {
                required: "Este campo es requerido para crear tu Test"
            }
        }
    });

});