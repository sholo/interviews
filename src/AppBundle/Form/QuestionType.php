<?php
/**
 * Created by PhpStorm.
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Entity\TermDataRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //Information Entity
            ->add('question', TextType::class)
            ->add('type', ChoiceType::class, array(
                'choices' => array(
                    'Abierta' => 0,
                    'Vedadero/Falso' => 1,
                    'Casillas Verificacion' => 2,
                ),
            ))
            ->add('score', ChoiceType::class, array(
                'choices' => array(
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6,
                    '7' => 7,
                    '8' => 8,
                    '9' => 9,
                    '10' => 10,
                ),
                ))
            // Status is by default Active, this is in Question.php Entity Construct
            ->add('status', ChoiceType::class, array(
                'choices' => array(
                    'Inactivo' => 0,
                    'Activo' => 1,
                    'Borrado' => 2,
                ),
            ))
            ->add('save', SubmitType::class, array('label' => ' GUARDAR '))
            ->getForm();


        $builder->add('answer', CollectionType::class, array(
            'entry_type' => AnswerType::class
        ));

        $builder->add('termData', EntityType::class, array(
            'class' => 'AppBundle:TermData',
            'query_builder' => function (TermDataRepository $er) {
                return $er->createQueryBuilder('u')
                    ->where('u.vocabulary = 1')
                    ->orderBy('u.name', 'ASC');
            },
            'choice_label' => 'name',
            'multiple' => true,
            'expanded' => false,
            'choice_value'=>'id',
        ));

        $builder->add('tags', EntityType::class, array(
            'class' => 'AppBundle:TermData',
            'query_builder' => function (TermDataRepository $er) {
                return $er->createQueryBuilder('u')
                    ->where('u.vocabulary = 2')
                    ->orderBy('u.name', 'ASC');
            },
            'choice_label' => 'name',
            'multiple' => true,
            'expanded' => false,
            'choice_value'=>'id'

        ));

        $builder->add('nivel', EntityType::class, array(
            'class' => 'AppBundle:TermData',
            'query_builder' => function (TermDataRepository $er) {
                return $er->createQueryBuilder('u')
                    ->where('u.vocabulary = 3')
                    ->orderBy('u.name', 'ASC');
            },
            'choice_label' => 'name',
            'multiple' => true,
            'expanded' => false,
            'choice_value'=>'id'

        ));

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Question',
        ));
    }
}