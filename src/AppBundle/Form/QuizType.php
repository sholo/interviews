<?php
/**
 * Created by PhpStorm.
 * User: magnolia
 * Date: 10/6/16
 * Time: 11:10 AM
 */

namespace AppBundle\Form;

use AppBundle\Entity\QuestionnaireRepository;
use AppBundle\Entity\QuestionRepository;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DomCrawler\Field\TextareaFormField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuizType extends  AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question',EntityType::class, array(
                'class' => 'AppBundle:Question',
                'query_builder' => function (QuestionRepository $er) {
                    return $er->createQueryBuilder('u');
                },
                'choice_label' => 'question',
               // 'multiple' => true,
                //'expanded' => true
                )
            )
            ->add('answer',TextType::class)
            ->add('answerOpen',TextareaType::class)
            ->getForm()
        ;

    }
    public  function  configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Evaluation',
        ));
    }

}
