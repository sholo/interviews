<?php
/**
 * Created by PhpStorm.
 * User: jose luis
 * Date: 20/09/2016
 * Time: 12:27 AM
 */

namespace AppBundle\Form;

use AppBundle\Entity\TermDataRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            // Machine Name is set by the function setMachineName in Entity
            ->add('description', TextType::class)
            ->add('status', ChoiceType::class, array(
                'choices'  => array(
                    'Inactivo' => 0,
                    'Activo' => 1,
                    'Borrado' => 2,
                ),))
            ->add('save', SubmitType::class, array('label' => 'GUARDAR'))
            ->getForm();

        $builder->add('termData',EntityType::class, array(
            'class' => 'AppBundle:TermData',
            'query_builder' => function (TermDataRepository $er) {
                return $er->createQueryBuilder('u')
                    ->where('u.vocabulary = 1')
                    ->orderBy('u.name', 'ASC');
            },
            'choice_label' => 'name',
            'multiple' => true,
            'expanded' => false,
            'choice_value'=>'id',
        ));

        $builder->add('tags', EntityType::class, array(
            'class' => 'AppBundle:TermData',
            'query_builder' => function (TermDataRepository $er) {
                return $er->createQueryBuilder('u')
                    ->where('u.vocabulary = 2')
                    ->orderBy('u.name', 'ASC');
            },
            'choice_label' => 'name',
            'multiple' => true,
            'expanded' => false,
            'choice_value'=>'id'

        ));

        $builder->add('nivel', EntityType::class, array(
            'class' => 'AppBundle:TermData',
            'query_builder' => function (TermDataRepository $er) {
                return $er->createQueryBuilder('u')
                    ->where('u.vocabulary = 3')
                    ->orderBy('u.name', 'ASC');
            },
            'choice_label' => 'name',
            'multiple' => true,
            'expanded' => false,
            'choice_value'=>'id'

        ));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Questionnaire',
        ));
    }
}