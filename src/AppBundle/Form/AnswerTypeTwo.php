<?php
/**
 * Created by PhpStorm.
<<<<<<< HEAD
<<<<<<< HEAD
 * User: eselejuanito
 * Date: 24/08/2016
 * Time: 09:46 AM
=======
 * User: magnolia
 * Date: 9/15/16
 * Time: 12:37 PM
>>>>>>> preguntafront
=======
 * User: magnolia
 * Date: 9/15/16
 * Time: 12:37 PM
>>>>>>> preguntafront
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnswerTypeTwo extends AbstractType
{
    public  function  buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder
           ->add('answer',TextType::class)
           ->add('right', CheckboxType::class, array(
               'required' => false,))
           // Status is by default Active, this is in Answer.php Entity Construct
           ->add('status', ChoiceType::class, array(
               'choices'  => array(
                   'Inactivo' => 0,
                   'Activo' => 1,
                   'Borrado' => 2,
               ),
           ))
           ->add('save', SubmitType::class, array('label' => 'GUARDAR'))
           ->getForm();
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Answer',
        ));
    }
}