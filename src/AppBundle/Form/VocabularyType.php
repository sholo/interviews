<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 22/08/2016
 * Time: 11:17 AM
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class VocabularyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            // Machine Name is set by the function setMachineName in Entity
            ->add('description', TextType::class)
            // Status is by default Active, this is in Vocabulary.php Entity Construct
            ->add('status', ChoiceType::class, array(
                'choices'  => array(
                    'Inactivo' => 0,
                    'Activo' => 1,
                    'Borrado' => 2,
                ),))
            ->add('visibility', ChoiceType::class, array(
                'choices'  => array(
                    'No Visible' => 0,
                    'Visible' => 1,
                ),))
            ->add('save', SubmitType::class, array('label' => 'GUARDAR'))
            ->getForm();
    }
}