<?php
/**
 * Created by PhpStorm.
 * User: Jose Luis
 * Date: 25/08/2016
 * Time: 11:47 PM
 */

namespace AppBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TermDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            // Machine Name is set by the function setMachineName in Entity
            ->add('description', TextType::class)
            // Status is by default Active, this is in TermData.php Entity Construct
            ->add('status', ChoiceType::class, array(
                'choices'  => array(
                    'Inactivo' => 0,
                    'Activo' => 1,
                    'Borrado' => 2,
                ),))
            ->add('save', SubmitType::class, array('label' => 'Guardar'))
            ->getForm();
    }
   
}