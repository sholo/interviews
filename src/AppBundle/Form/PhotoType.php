<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 01/09/2016
 * Time: 10:57 AM
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class PhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            //Information Entity
            ->add('name', FileType::class, array('label' => 'Foto'))
            ->add('save', SubmitType::class, array('label' => ' '))
            ->getForm();
    }
}