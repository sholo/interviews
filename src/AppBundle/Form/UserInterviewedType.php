<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 24/08/2016
 * Time: 09:46 AM
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class UserInterviewedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //User Entity
            ->add('email', EmailType::class)
            // Status is by default Active, this is in User.php Entity Construct
            ->add('status', ChoiceType::class, array(
                'choices'  => array(
                    'Inactivo' => 0,
                    'Activo' => 1,
                    'Borrado' => 2,
                ),))

            ->add('save', SubmitType::class, array('label' => ' '))
            ->getForm();
    }
}