<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 22/08/2016
 * Time: 11:17 AM
 */

namespace AppBundle\Form;

use AppBundle\Entity\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class TestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('status', ChoiceType::class, array(
                'choices'  => array(
                    'Inactivo' => 0,
                    'Activo' => 1,
                    'Borrado' => 2,
                ),))
            ->add('date_expiry_from', TextType::class)
            ->add('date_expiry_to', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'GUARDAR'))
            ->getForm();

        $builder->add('user', EntityType::class, array(
            'class' => 'AppBundle:User',
            'query_builder' => function (UserRepository $er) {
                return $er->createQueryBuilder('u')
                    ->orderBy('u.username', 'ASC');
            },
            'choice_label' => 'email',
            'multiple' => true,
            'expanded' => false,
            'choice_value'=>'id',
        ));
    }
}