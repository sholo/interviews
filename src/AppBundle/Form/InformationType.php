<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 24/08/2016
 * Time: 10:49 AM
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class InformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            //Information Entity
            ->add('first_name', TextType::class)
            ->add('last_name', TextType::class)
            ->add('mobile_phone', TextType::class)
            ->add('phone', TextType::class)
            ->add('level_study', TextType::class)
            ->add('living_in', TextType::class)
            ->add('birthdate', TextType::class)

            ->add('civil_status', ChoiceType::class, array(
                'choices'  => array(
                    'Soltero/a' => 'soltero',
                    'Comprometido/a' => 'comprometido',
                    'Casado/a' => 'casado',
                    'Divorciado/a' => 'divorciado',
                    'Viudo/a' => 'viudo',
                ),
            ))

            ->add('save', SubmitType::class, array('label' => ' '))
            ->getForm();
    }
}