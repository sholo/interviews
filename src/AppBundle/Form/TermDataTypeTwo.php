<?php
/**
 * Created by PhpStorm.
 * User: Jose Luis
 * Date: 25/08/2016
 * Time: 11:47 PM
 */

namespace AppBundle\Form;
use AppBundle\Entity\TermDataRepository;
use AppBundle\Entity\TermRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TermDataTypeTwo extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', EntityType::class, array(
                // query choices from this entity
                'class' => 'AppBundle:TermData',
                'query_builder' => function (TermDataRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.vocabulary', 'ASC');
                },

                // property as the visible option string
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => false,
                )
            );
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TermData',
        ));
    }
}