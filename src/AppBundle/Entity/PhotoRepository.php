<?php

namespace AppBundle\Entity;
use Doctrine\ORM\EntityRepository;

/**
 * PhotoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PhotoRepository extends EntityRepository
{
    const REPOSITORY = 'AppBundle:Photo';

    /**
     * Get findOnePhotoByField
     *
     * @param int $user_id
     *
     * @return array
     */
    public function findOnePhotoByUser($user_id)
    {
        $result = $this->getEntityManager()
            ->getRepository($this::REPOSITORY)
            ->findOneByUser($user_id);

        return $result;
    }
}
