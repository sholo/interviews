<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @ORM\Table(name="user")
 *
 * Defines the properties of the User entity to represent the application users.
 * See http://symfony.com/doc/current/book/doctrine.html#creating-an-entity-class
 *
 * Tip: if you have an existing database, you can generate these entity class automatically.
 * See http://symfony.com/doc/current/cookbook/doctrine/reverse_engineering.html
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class User implements UserInterface
{
    //Constant to use it with the field status to create a new Question
    const STATUS_INACTIVATE = 0;
    const STATUS_ACTIVATE = 1;
    const STATUS_DELETE = 2;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->test = new \Doctrine\Common\Collections\ArrayCollection();
        $this->termData = new \Doctrine\Common\Collections\ArrayCollection();

        //By Default, An User should be created like an active user
        $this->status = self::STATUS_ACTIVATE;
        $this->roles = ["PROSPECT"];
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     *
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es un email valido.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     *
     * @ORM\Column(name="status",  type="boolean", nullable=false)
     *
     * @Assert\Type(
     *     type="integer",
     *     message="El valor {{ value }} no es del tipo Entero"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 2,
     *      minMessage = "Usted debe escoger un numero mayor o igual a {{ limit }}",
     *      maxMessage = "Usted debe escoger un numero menor o igual a {{ limit }}"
     * )
     */
    private $status;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = [];

    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Test", mappedBy="user")
     */
    private $test;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\TermData", mappedBy="user")
     */
    private $termData;

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }
    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles()
    {
        $roles = $this->roles;

        // guarantees that a user always has at least one role for security
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     */
    public function getSalt()
    {
        // See "Do you need to use a Salt?" at http://symfony.com/doc/current/cookbook/security/entity_provider.html
        // we're using bcrypt in security.yml to encode the password, so
        // the salt value is built-in and you don't have to generate one

        return;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials()
    {
        // if you had a plainPassword property, you'd nullify it here
        // $this->plainPassword = null;
    }
    /**
     * Add test
     *
     * @param \AppBundle\Entity\Test $test
     *
     * @return User
     */
    public function addTest(\AppBundle\Entity\Test $test)
    {
        $this->test[] = $test;

        return $this;
    }

    /**
     * Remove test
     *
     * @param \AppBundle\Entity\Test $test
     */
    public function removeTest(\AppBundle\Entity\Test $test)
    {
        $this->test->removeElement($test);
    }

    /**
     * Get test
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * Add termDatum
     *
     * @param \AppBundle\Entity\TermData $termDatum
     *
     * @return User
     */
    public function addTermDatum(\AppBundle\Entity\TermData $termDatum)
    {
        $this->termData[] = $termDatum;

        return $this;
    }

    /**
     * Remove termDatum
     *
     * @param \AppBundle\Entity\TermData $termDatum
     */
    public function removeTermDatum(\AppBundle\Entity\TermData $termDatum)
    {
        $this->termData->removeElement($termDatum);
    }

    /**
     * Get termData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTermData()
    {
        return $this->termData;
    }

    /**
     * Get textStatus
     *
     * @return string
     */
    public function getTextStatus($status)
    {
        switch ($status)
        {
            case self::STATUS_INACTIVATE:  return "Inactivo";
            case self::STATUS_ACTIVATE:  return "Activo";
            case self::STATUS_DELETE:  return "Borrado";
            default: return "Estado No Existente";
        }
    }

    public function arrayToStringRoles($roles)
    {
        return implode(",", $roles);
    }

}
