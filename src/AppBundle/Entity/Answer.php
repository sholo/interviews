<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 *
 * @ORM\Table(name="answer", indexes={@ORM\Index(name="fk_answer_question1_idx", columns={"question_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AnswerRepository")
 */
class Answer
{
    //Constant to use it with the field status to create a new Question
    const STATUS_INACTIVATE = 0;
    const STATUS_ACTIVATE = 1;
    const STATUS_DELETE = 2;

    //Constant to use it with the field right to create a new Answer, with a right or wrong answer
    const WRONG = 0;
    const RIGHT = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text", length=65535, nullable=false)
     */
    private $answer;

    /**
     * @var integer
     *
     * @ORM\Column(name="`right`", type="integer", nullable=false)
     */
    private $right;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Question
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Question")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * })
     */
    private $question;

    /**
     * Constructor
     */
    public function __construct()
    {
        //By Default, An Answer should be created like an active answer
        $this->status = self::STATUS_ACTIVATE;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return Answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set right
     *
     * @param integer $right
     *
     * @return Answer
     */
    public function setRight($right)
    {
        $this->right = $right;

        return $this;
    }

    /**
     * Get right
     *
     * @return integer
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Answer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\Question $question
     *
     * @return Answer
     */
    public function setQuestion(\AppBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Get textStatus
     *
     * @param int $status
     *
     * @return string
     */
    public function getTextStatus($status)
    {
        switch ($status)
        {
            case self::STATUS_INACTIVATE:  return "Inactivo";
            case self::STATUS_ACTIVATE:  return "Activo";
            case self::STATUS_DELETE:  return "Borrado";
            default: return "Estado No Existente";
        }
    }

    /**
     * Get textRight
     *
     * @param int $right
     *
     * @return string
     */
    public function getTextRight($right)
    {
        switch ($right)
        {
            case self::RIGHT:  return "Correcta";
            case self::WRONG:  return "Incorrecta";
            default: return "Falta Definir Respuesta";
        }
    }

    /**
     * Get saveAnswer
     *
     * @param string $answer
     * @param int $right
     * @param \AppBundle\Entity\Question $question
     *
     * @return Answer
     */
    public function saveAnswer($answer, $right, $question)
    {

        $this->answer = $answer;
        $this->right = $right;
        $this->question = $question;

        return $this;
    }

}
