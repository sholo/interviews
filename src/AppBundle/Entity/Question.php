<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\QuestionRepository")
 *
 */
class Question
{
    //Constant to use it with the field status to create a new Question
    const STATUS_INACTIVATE = 0;
    const STATUS_ACTIVATE = 1;
    const STATUS_DELETE = 2;

    //Constant to use it with the field type to create a new Question, with a kind of question
    const TYPE_OPEN = 0;
    const TYPE_TRUE_FALSE = 1;
    const TYPE_CHECKBOXES = 2;
    const TYPE_MULTIPLE = 3;
    const TYPE_COMPLETE = 4;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="text", length=65535, nullable=false)
     */
    private $question;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="integer", nullable=false)
     */
    private $score;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\TermData", inversedBy="question")
     * @ORM\JoinTable(name="question_term_data",
     *   joinColumns={
     *     @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="term_data_id", referencedColumnName="id")
     *   }
     * )
     */
    private $termData;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Questionnaire", inversedBy="question")
     * @ORM\JoinTable(name="question_questionnaire",
     *   joinColumns={
     *     @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="questionnaire_id", referencedColumnName="id")
     *   }
     * )
     */
    private $questionnaire;

    protected $answer;

    protected  $tags;

    protected  $nivel;

    /**
     * @return mixed
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * @param mixed $nivel
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param ArrayCollection $answer
     */
    public function setAnswer(Answer $answer = null)
    {
        $this->answer = $answer;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->termData = new ArrayCollection();
        $this->questionnaire = new ArrayCollection();
        //By Default, An Question should be created like an active question
        $this->status = self::STATUS_ACTIVATE;
        $this->answer = new ArrayCollection();
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Question
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return Question
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Question
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add termDatum
     *
     * @param \AppBundle\Entity\TermData $termDatum
     *
     * @return Question
     */
    public function addTermDatum(\AppBundle\Entity\TermData $termDatum)
    {
        $termDatum->addQuestion($this);
        $this->termData[] = $termDatum;

        return $this;
    }

    /**
     * Remove termDatum
     *
     * @param \AppBundle\Entity\TermData $termDatum
     *
     * @return Question
     */
    public function removeTermDatum(\AppBundle\Entity\TermData $termDatum)
    {
        $termDatum->removeQuestion($this);
        $this->termData->removeElement($termDatum);

        return $this;
    }

    /**
     * Get termData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTermData()
    {
        return $this->termData;
    }

    /**
     * Add questionnaire
     *
     * @param \AppBundle\Entity\Questionnaire $questionnaire
     *
     * @return Question
     */
    public function addQuestionnaire(\AppBundle\Entity\Questionnaire $questionnaire)
    {
        $this->questionnaire[] = $questionnaire;

        return $this;
    }

    /**
     * Remove questionnaire
     *
     * @param \AppBundle\Entity\Questionnaire $questionnaire
     */
    public function removeQuestionnaire(\AppBundle\Entity\Questionnaire $questionnaire)
    {
        $this->questionnaire->removeElement($questionnaire);
    }

    /**
     * Get questionnaire
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionnaire()
    {
        return $this->questionnaire;
    }

    /**
     * Get textStatus
     *
     * @param int $status
     *
     * @return string
     */
    public function getTextStatus($status)
    {
        switch ($status)
        {
            case self::STATUS_INACTIVATE:  return "Inactivo";
            case self::STATUS_ACTIVATE:  return "Activo";
            case self::STATUS_DELETE:  return "Borrado";
            default: return "Estado No Existente";
        }
    }

    /**
     * Get TextType
     *
     * @param int $type
     *
     * @return string
     */
    public function getTextType($type)
    {
        switch ($type)
        {
            case self::TYPE_OPEN:  return "Abierta";
            case self::TYPE_MULTIPLE:  return "Opcion Multiple";
            case self::TYPE_TRUE_FALSE:  return "Verdadero/Falso";
            case self::TYPE_CHECKBOXES:  return "Casillas de Verificacion";
            case self::TYPE_COMPLETE:  return "Completa la Oracion";
            default: return "Tipo No Existente";
        }
    }

    /**
     * Get saveQuestion
     *
     * @param string $question
     * @param int $score
     * @param int $type
     *
     * @return Question
     */
    public function saveQuestion($question, $score, $type)
    {
        $this->question = $question;
        $this->score = $score;
        $this->type = $type;

        return $this;
    }

    /**
     * Get addTerms
     *
     * @param TermData $terms
     *
     * @return Question
     */
    public function addTerms($terms)
    {
        unset($this->termData);

        foreach ($terms as $term)
        {
            $this->addTermDatum($term);
        }

        return $this;
    }
}
