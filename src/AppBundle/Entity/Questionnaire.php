<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Questionnaire
 *
 * @ORM\Table(name="questionnaire", indexes={@ORM\Index(name="fk_questionnaire_user1_idx", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\QuestionnaireRepository")
 */
class Questionnaire
{
    const STATUS_INACTIVATE =0;
    const STATUS_ACTIVATE =1;
    const STATATUS_DELETE =2;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\TermData", inversedBy="questionnaire")
     * @ORM\JoinTable(name="questionnaire_term_data",
     *   joinColumns={
     *     @ORM\JoinColumn(name="questionnaire_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="term_data_id", referencedColumnName="id")
     *   }
     * )
     */
    private $termData;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Question", mappedBy="questionnaire")
     */
    private $question;

    protected  $tags;

    protected  $nivel;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->status = self::STATUS_ACTIVATE;
        $this->termData = new \Doctrine\Common\Collections\ArrayCollection();
        $this->question = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Questionnaire
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Questionnaire
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Questionnaire
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Questionnaire
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add termDatum
     *
     * @param \AppBundle\Entity\TermData $termDatum
     *
     * @return Questionnaire
     */
    public function addTermDatum(\AppBundle\Entity\TermData $termDatum)
    {
        $this->termData[] = $termDatum;

        return $this;
    }

    /**
     * Remove termDatum
     *
     * @param \AppBundle\Entity\TermData $termDatum
     */
    public function removeTermDatum(\AppBundle\Entity\TermData $termDatum)
    {
        $this->termData->removeElement($termDatum);
    }

    /**
     * Get termData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTermData()
    {
        return $this->termData;
    }

    /**
     * Add question
     *
     * @param \AppBundle\Entity\Question $question
     *
     * @return Questionnaire
     */
    public function addQuestion(\AppBundle\Entity\Question $question)
    {
        $question->addQuestionnaire($this);
        $this->question[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \AppBundle\Entity\Question $question
     */
    public function removeQuestion(\AppBundle\Entity\Question $question)
    {
        $this->question->removeElement($question);
    }

    /**
     * Get question
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestion()
    {
        return $this->question;
    }
    public function getTextStatus($status)
    {
        if($status==0)
        {
            return "Inactivo";
        }
        elseif($status==1)
        {
            return "Activo";
        }
        elseif($status==2)
        {
            return "Borrado";
        }
    }
    /**
     * Get addQuestionnaire
     *
     * @param Questionnaire $quest
     *
     * @return Questionnaire
     */
    public function addQuestionnaire($quest)
    {
        foreach ($quest as $ques)
        {
            $this->addQuestion($ques);
        }

        return $this;
    }

    /**
     * Get addQuestions
     *
     * @param Question $cues
     *
     * @return Questionnaire
     */
    public function addQuestions($cues)
    {
        //$order = 1;
        foreach ($cues as $p)
        {
            //$this->setOrder($order);
            $this->addQuestion($p);
            //order++;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return mixed
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * @param mixed $nivel
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;
    }

    public function saveQuestionnaire($name, $description)
    {
        $this->name = $name;
        $this->description = $description;

        return $this;
    }

}
