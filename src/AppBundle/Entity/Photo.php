<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Photo
 *
 * @ORM\Table(name="photo", indexes={@ORM\Index(name="fk_photo_user1_idx", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PhotoRepository")
 */
class Photo
{
    //Constant to use it with the field status to create a new Photo
    const STATUS_INACTIVATE = 0;
    const STATUS_ACTIVATE = 1;
    const STATUS_DELETE = 2;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank(message="Por favor ingresa una foto")
     * @Assert\File(
     *     maxSize = "5000k",
     *     mimeTypes = {"image/*"},
     *     mimeTypesMessage = "Por favor ingresa una foto en formato JPG, PNG o GIF"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     */
    private $path;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        //By Default, An User should be created like an active user
        $this->status = self::STATUS_ACTIVATE;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Photo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Photo
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Photo
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Photo
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UploadedFile $photo
     * @return string $file_name
     */
    public function photoManager(UploadedFile $photo, $path, $same_name = null)
    {
        // Generate a unique name for the file before saving it
        $file_name = count($same_name) == 2 ? $same_name[0].'.'.$photo->guessExtension() : md5(uniqid()).'.'.$photo->guessExtension();

        // Move the file to the directory where user photos are stored
        $photo->move(
            $path,
            $file_name
        );

        return $file_name;
    }
}
