<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 10/08/2016
 * Time: 11:44 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Test;
use AppBundle\Entity\User;
use AppBundle\Form\TestType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * @Route("/test")
 */
class TestController extends Controller
{

    const REPOSITORY = 'AppBundle:Test';

    /**
     * @Route("/", name="show_all_tests")
     */
    public function showAllTests()
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findAllTests();

        return $this->render( 'test/show_all.html.twig', [
            'result' => $result
        ]);
    }

    /**
     * @Route("/crear/", name="create_test")
     */
    public function createTest(Request $request)
    {
        $test = new Test();
        $user = new User();
        $test ->getUser()->add($user);

        $form = $this->createForm(TestType::class, $test);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $request->get('test') != null)
        {
            $t = $request->get('test');

            if ( array_key_exists('date_expiry_from', $t) && array_key_exists('date_expiry_to', $t) )
            {
                if (strtotime($t['date_expiry_to']) - strtotime($t['date_expiry_from']) >= 0)
                {
                    $test->saveTest(
                        $t['name'],
                        $t['description'],
                        new \DateTime($t['date_expiry_from']),
                        new \DateTime($t['date_expiry_to'])
                    );
                }
            }
            else
            {
                $test->saveTest(
                    $t['name'],
                    $t['description']
                );
            }

            if (array_key_exists('user', $t))
            {
                $registered_users = array();
                $new_users = array();
                $ru_email = array();
                $nu = array();

                foreach ($t['user'] as $user)
                {
                    if ( is_numeric ($user) )
                    {
                        $registered_users[] = (int)$user;
                    }
                    elseif (filter_var($user, FILTER_VALIDATE_EMAIL) !== false)
                    {
                        $new_users[] = $user;
                    }
                }

                if ($new_users)
                {
                    foreach ($new_users as $u)
                    {
                        $user = new User();
                        $user->setPassword($u);
                        $user->setEmail($u);
                        $user->setUsername($u);

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($user);
                        $em->flush();

                        $nu[] = $user;
                        $user = null;
                    }
                }

                if ($registered_users)
                {

                    $registered_users = $this->getDoctrine()
                        ->getRepository('AppBundle:User')
                        ->findUsersByIds($registered_users);

                    foreach ($registered_users as $ru)
                    {
                        $ru_email[] = $ru->getEmail();
                    }

                }

                $all_users = array_merge($nu, $registered_users);

                $all_users_emails = array_merge($new_users, $ru_email);
                $this->sendEmail($all_users_emails);

            }

            $test->addUsers($all_users);
            $em = $this->getDoctrine()->getManager();
            $em->persist($test);
            $em->flush();

            return $this->redirectToRoute('show_all_tests');
        }

        return $this->render('test/create.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    public function sendEmail($emails)
    {

        $message = \Swift_Message::newInstance()
            ->setSubject('Hola Amigo, ¿Estas listo para probar tus Habilidades?')
            ->setFrom('juancfg_18@hotmail.com')
            ->setTo($emails)
            ->setBody(
                $this->renderView(
                    'test/email.html.twig'
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);

    }

}