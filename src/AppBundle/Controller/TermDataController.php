<?php
/**
 * Created by PhpStorm.
 * User: Jose Luis
 * Date: 24/08/2016
 * Time: 05:27 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\TermData;
use AppBundle\Entity\Vocabulary;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\TermDataType;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/termino")
 */
class TermDataController extends Controller
{
    const REPOSITORY = 'AppBundle:TermData';

    /**
     * @Route("/", name="show_all_terms")
     */
    public function showAllTerms()
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findAllTerm();

        if (!$result) {
            return $this->render('error/404.html.twig', [
                'result' => $result
            ]);
        } else {
            return $this->render('term/show_all.html.twig', [
                'result' => $result
            ]);
        }
    }

    /**
     * @Route("/{vocabulary}", name="show_by_MachineName")
     */
    public function showTermByMachineName($vocabulary)
    {
        $voc = $this->getDoctrine()
            ->getRepository('AppBundle:Vocabulary')
            ->findbyMachineName($vocabulary);

        if(!$voc)
        {
            return $this->render('error/404.html.twig');
        }

        $result = $this->getDoctrine()
            ->getRepository('AppBundle:TermData')
            ->findTermsByVocabularies($vocabulary);

        return $this->render('term/showbyMachineName_all.html.twig', [
            'result' => $result,
            'vocabulary' => $vocabulary
        ]);

    }
    /**
     * @Route("/ver/{id}/", name="show_by_Details")
     */
    public function showTerm($id)
    {
        $result = $this->getDoctrine()
            ->getRepository('AppBundle:TermData')
            ->find($id);
        if (!$result) {
            return $this->render('term/resultNotTags.html.twig', [
                'result' => $result
            ]);
        } else {
            return $this->render('term/show.html.twig', [
                'result' => $result
            ]);
        }
    }

    /**
     * @Route("/editar/{id}/", name="edit_terms")
     */
    public function edit($id, Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneTermById($id);

        if(!$result)
        {
            return $this->render('error/404.html.twig');
        }
        $form = $this->createForm(TermDataType::class, $result);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $result->setMachineName($result->getName());

            $edi = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($edi);
            $em->flush();
            return $this->redirectToRoute('show_by_Details', array('id' => $id));
        }
        return $this->render('term/edit.html.twig', array(
            'form' => $form->createView(),
            'id' => $id
        ));
    }

    /**
     * @Route("/crear/{machineName}", name="create_terms")
     */
    public function create($machineName, Request $request)
    {
        $voc = $this->getDoctrine()
            ->getRepository('AppBundle:Vocabulary')
            ->findbyMachineName($machineName);

        if (!$voc) {
            return $this->render('error/404.html.twig');
        }
        $term = new TermData();
        $form = $this->createForm(TermDataType::class, $term);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $term->setMachineName($term->getName());
            $term = $form->getData();
            $term->setVocabulary($voc);

            $em = $this->getDoctrine()->getManager();
            $em->persist($term);
            $em->flush();
            return $this->redirectToRoute('show_all_terms');
        }
        return $this->render('term/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/borrar/{id}/", name="delete_terms")
     */
    public function delete($id, Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneTermById($id);

        if(!$result)
        {
            return $this->render('error/404.html.twig');
        }
        $result->setStatus(TermData::STATUS_INACTIVATE);

        $form = $this->createFormBuilder($result)
            ->add('delete', SubmitType::class, array('label' => 'Si'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $te = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($te);
            $em->flush();

            return $this->redirectToRoute('show_by_Details', array('id' => $id));
        }
        return $this->render('term/delete.html.twig', array(
            'form' => $form->createView(),
            'result' => $result
        ));
    }
}