<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 25/08/2016
 * Time: 10:20 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Information;
use AppBundle\Entity\User;
use AppBundle\Form\InformationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/usuario/informacion")
 */
class InformationController extends Controller
{
    const REPOSITORY = 'AppBundle:Information';

    /**
     * @Route("/crear/{user_id}/", name="create_user_information")
     */
    public function createInformation($user_id, Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneUserById($user_id);

        // Checking if the user ID exists, if not, we need to redirect
        if (!$user)
        {
            return $this->render('error/404.html.twig');
        }
        // If the user ID exists, we need to check if it isn't a user type employee
        else
        {
            if ($user->arrayToStringRoles($user->getRoles()) == "EMPLOYEE")
            {
                return $this->redirectToRoute('show_all_users');
            }
        }

        // Checking if the user doesn't has information, if it's right, we need to redirect to show all users
        $information = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneInformationByUser($user_id);

        if ($information)
        {
            return $this->redirectToRoute('edit_user_information', array('user_id' => $user_id));
        }

        $information = new Information();

        $form = $this->createForm(InformationType::class, $information);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $information->setUser($user);

            $information = $form->getData();
            $information->setBirthdate(new \DateTime($information->getBirthdate()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($information);
            $em->flush();

            return $this->redirectToRoute('show_all_users');
        }

        return $this->render('information/create.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/editar/{user_id}/", name="edit_user_information")
     */
    public function editInformation($user_id, Request $request)
    {

        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneUserById($user_id);

        // Checking if the user ID exists, if not, we need to redirect to create user
        if (!$user)
        {
            return $this->render('error/404.html.twig');
        }
        // If the user ID exists, we need to check if it isn't a user type employee
        else
        {
            if ($user->arrayToStringRoles($user->getRoles()) == "EMPLOYEE")
            {
                return $this->redirectToRoute('show_all_users');
            }
        }

        $information = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneInformationByUser($user_id);

        if (!$information)
        {
            return $this->render('error/404.html.twig');
        }

        $information->dateToString($information->getBirthdate());

        $form = $this->createForm(InformationType::class, $information);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $information = $form->getData();
            $information->setBirthdate(new \DateTime($information->getBirthdate()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($information);
            $em->flush();

            return $this->redirectToRoute('show_all_users');
        }

        return $this->render('information/edit.html.twig', array(
            'form' => $form->createView(),
        ));

    }
}