<?php
/**
 * Created by PhpStorm.
 * User: magnolia
 * Date: 10/6/16
 * Time: 10:42 AM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Answer;
use AppBundle\Entity\Evaluation;
use AppBundle\Entity\Question;
use AppBundle\Form\QuizType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class EvaluationController extends  Controller
{
    /**
     * @Route("/evaluacion/{id}" ,name="show_quiz")
     */

    public function showQuiz($id,Request $request)
    {
        $question = $this ->getDoctrine()
            ->getRepository('AppBundle:Questionnaire')
            ->findQuestionsByQuestionnaire($id);

        $answers = $this->getDoctrine()
            ->getRepository('AppBundle:Answer')
            ->findAnswersByQuestion($question);

        return $this->render('evaluation/quiz.html.twig',array(
            'questions'=>$question,
            'answers' => $answers
        ));
    }

    /**
     * @Route("/evaluacion/guardar",name="save_quiz")
     */

    public function saveQuiz(Request $request)
    {
        $evaluation = new  Evaluation();

        if($request->request->get('question') != null )
        {
            return $this->render('evaluation/temporal.html.twig', array(
                'request' => $request,
            ));

            $e = $request->request->get('question');

            if($e['answerOpen'])
            {
                $evaluation ->saveEvaluation(
                    $e['question'],
                    $e['answerOpen']
                );

                $em = $this ->getDoctrine()->getManager();
                $em->persist($evaluation);
                $em->flush();
            }

            if($e['answerVF'])
            {

            }

            //Checkbox: 1 to 4 answers
            if($e['answer'])
            {
                foreach ($e['answer'] as $item)
                {

                }
            }

        }
        return $this ->redirectToRoute('show_all_questionnaire');
    }


}