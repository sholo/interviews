<?php
/**
 * Created by PhpStorm.
 * User: Jose Luis
 * Date: 07/09/2016
 * Time: 10:12 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Form\QuestionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/pregunta")
 */

class QuestionController extends Controller
{
    const REPOSITORY = 'AppBundle:Question';

    /**
     * @Route("/", name="show_all_questions")
     */
    public  function showAllQuestions()
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findAllQuestions();

        return $this->render('question/show_all.html.twig', ['result' => $result]);
    }

    /**
     * @Route("/ver/{id}", name="show_question")
     */
    public function showQuestion($id)
    {
        $question = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneQuestionById($id);

        if (!$question)
        {
            return $this->render('error/404.html.twig');
        }

        $answers = $this->getDoctrine()
            ->getRepository('AppBundle:Answer')
            ->findAnswersByQuestion($question);

        $termdata = $this->getDoctrine()
            ->getRepository('AppBundle:TermData')
            ->findTermsByQuestion($id);

        return $this->render('question/show.html.twig', [
                'question' => $question,
                'answers' => $answers,
                'termdata' => $termdata
            ]
        );
    }

    /**
     * @Route("/crear/", name="create_question")
     */
    public function createQuestion(Request $request)
    {
        //Una pregunta solo podra recibir 4 respuestas para casillas de verificacion
        $question = new  Question();

        $answer1 = new Answer();
        $question ->getAnswer()->add($answer1);

        $answer2 = new Answer();
        $question ->getAnswer()->add($answer2);

        $answer3 = new Answer();
        $question ->getAnswer()->add($answer3);

        $answer4 = new Answer();
        $question ->getAnswer()->add($answer4);

        $form = $this->createForm(QuestionType::class, $question);

        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() && $request->request->get('question') != null )
        {

            $q = $request->request->get('question');

            // Creating and saving a Question
            $question->saveQuestion(
                $q['question'],
                (int)$q['score'],
                (int)$q['type']
            );

            $em = $this->getDoctrine()->getManager();
            $terms_id = array_merge($q['termData'], $q['tags'], $q['nivel']);
            foreach ($terms_id as $id)
            {
                $array_terms_ids[] = (int)$id;
            }

            $terms = $this->getDoctrine()
                ->getRepository('AppBundle:TermData')
                ->findTermsByIds($array_terms_ids);

            $question->addTerms($terms);
            $question->setStatus(Question::STATUS_ACTIVATE);

            $em->persist($question);
            $em->flush();

            if ( $q['answer'] )
            {
                foreach ($q['answer'] as $a)
                {
                    $answer_name = $a['answer'];
                    if ($answer_name != null)
                    {
                        $answer_right = isset($a['right'])? (int)$a['right'] : 0;

                        // Creating a Answer
                        $answer = new Answer();

                        $answer->saveAnswer(
                            $answer_name,
                            $answer_right,
                            $question
                        );

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($answer);
                        $em->flush();
                    }
                }
            }

            return new JsonResponse('success');

        }

        return $this->render('question/create.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/borrar/{id}/", name="delete_question")
     */
    public function deleteUser($id, Request $request)
    {

        $question = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneQuestionById($id);

        if (!$question)
        {
            return $this->render('error/404.html.twig');
        }

        $question->setStatus(Question::STATUS_INACTIVATE);

        $form = $this->createFormBuilder($question)
            ->add('delete', SubmitType::class, array('label' => 'Si'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $question = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();

            return $this->redirectToRoute('show_question', array('id' => $id));
        }

        return $this->render('question/delete.html.twig', array(
            'form' => $form->createView(),
            'question' => $question
        ));

    }

    /**
     * @Route("/editar/{id}/", name="edit_question")
     */
    public function editQuestion($id, Request $request)
    {
        $tecnologias = array();
        $niveles = array();
        $tags = array();

        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneQuestionById($id);

        if (!$result)
        {
            return $this->render('error/404.html.twig');
        }

        $answers = $this->getDoctrine()
            ->getRepository('AppBundle:Answer')
            ->findAnswersByQuestion($result);

        //Una pregunta solo podra recibir 4 respuestas para casillas de verificacion
        $question = new Question();
        foreach ($answers as $answer)
        {
            $answer_right = $answer->getRight() == 1 ? TRUE : FALSE;
            $answer->setRight($answer_right);
            $question ->getAnswer()->add($answer);
        }

        $question->setQuestion($result->getQuestion());
        $question->setType($result->getType());
        $question->setScore($result->getScore());
        $question->setStatus($result->getStatus());

        $terms_of_question = $this->getDoctrine()
            ->getRepository('AppBundle:TermData')
            ->findTermsByQuestion($id);

        foreach ($terms_of_question as $t)
        {
            if($t->getVocabulary()->getId() == 1)
            {
                $tecnologias[] = $t->getId();
            }
            elseif ($t->getVocabulary()->getId() == 2)
            {
                $tags[] = $t->getId();
            }
            elseif ($t->getVocabulary()->getId() == 3)
            {
                $niveles[] = $t->getId();
            }
        }

        $form = $this->createForm(QuestionType::class, $question);

        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() && $request->request->get('question') != null )
        {

            $q = $request->request->get('question');

            // Creating and saving a Question
            $result->saveQuestion(
                $q['question'],
                (int)$q['score'],
                (int)$q['type']
            );

            $em = $this->getDoctrine()->getManager();
            $terms_id = array_merge($q['termData'], $q['tags'], $q['nivel']);
            $array_terms_ids = array();
            foreach ($terms_id as $id)
            {
                $array_terms_ids[] = (int)$id;
            }

            $original_terms = array_merge($tecnologias, $tags, $niveles);

            $terms = array_merge($array_terms_ids, $original_terms);
            $terms = array_unique($terms);
            $remove_terms = array_diff( $original_terms, $array_terms_ids );

            foreach ($remove_terms as $rt)
            {
                if (($key = array_search($rt, $terms)) !== NULL)
                {
                    unset($terms[$key]);
                }
            }

            $add_terms = $this->getDoctrine()
                ->getRepository('AppBundle:TermData')
                ->findTermsByIds($terms);

            $result->addTerms($add_terms);

            $result->setStatus(Question::STATUS_ACTIVATE);

            $em->persist($result);
            $em->flush();

            if ( $q['answer'] )
            {

                for ($i = 0; $i < count($answers); $i++)
                {
                    $answer_name = $q['answer'][$i]['answer'];
                    $answer_right = isset($q['answer'][$i]['right'])? (int)$q['answer'][$i]['right'] : 0;

                    $answers[$i]->saveAnswer(
                        $answer_name,
                        $answer_right,
                        $result
                    );

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($answers[$i]);
                    $em->flush();
                }
            }

            return $this->redirectToRoute('show_all_questions');

        }

        return $this->render('question/edit.html.twig', array(
            'form' => $form->createView(),
            'tecnologias' => $tecnologias,
            'niveles' => $niveles,
            'tags' => $tags,
        ));

    }

}