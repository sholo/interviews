<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 09/08/2016
 * Time: 09:59 AM
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class PrimerController extends Controller
{
    /**
     * @Route("/hi/{nombre}")
     */
    public function showAction($nombre)
    {
        $notes = [
            'ASDAkasjdijasi',
            'lkajslkdjlkasjd',
            'lkaiosiosduoiusiouom'
        ];

        return $this->render('hi/show.html.twig', [
            'name'  => $nombre,
            'notes' => $notes
        ]);
    }

    /**
     * @Route("/hi/{nombre}/notes", name="hi_amigo")
     * @Method("GET")
     */
    public function getNotesAction()
    {
        $notes = [
            ['id' => 1, 'username' => 'ASDAkasjdijasi'],
            ['id' => 1, 'username' => 'lkajslkdjlkasjd'],
            ['id' => 1, 'username' => 'lkaiosiosduoiusiouom']
        ];

        $data = [
            'notes' => $notes,
        ];

        //return new Response(json_encode($data));
        return new JsonResponse($data);
    }

}