<?php
/**
 * Created by PhpStorm.
 * User: magnolia
 * Date: 10/12/16
 * Time: 10:20 PM
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    /**
     * @Route("/dashboard",name="show_dashboard")
     */
    public function  showDashboard()
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->countProspect();

        $questionnaires = $this->getDoctrine()
            ->getRepository('AppBundle:Questionnaire')
            ->countQuestionnaire();

        $test = $this->getDoctrine()
            ->getRepository('AppBundle:Test')
            ->countTest();

        return $this->render('dashboard/show.html.twig',array(
            'users'=>$users,
            'questionnaires'=>$questionnaires,
            'tests'=>$test
        ));

    }

}