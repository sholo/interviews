<?php
/**
 * Created by PhpStorm.
 * User: Jose Luis
 * Date: 19/09/2016
 * Time: 11:54 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Questionnaire;
use AppBundle\Form\QuestionnaireType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/cuestionario")
 */
class QuestionnaireController extends Controller
{
    const REPOSITORY = 'AppBundle:Questionnaire';

    /**
     * @Route("/",name="show_all_questionnaire")
     */
    public  function ShowQuestionnaire()
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->finAllQuestionnaire();

        return $this->render('questionnaire/show_all.html.twig', [
            'result' => $result
        ]);
    }

    /**
     * @Route("/ver/{id}", name="show_by_DetailsQuestionnaire")
     */
    public function showbyIdQuestionnaire($id)
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneQuestionnaireById($id);

        if(!$result) {
            return $this->render('error/404.html.twig');
        }

        $question = $this ->getDoctrine()
            ->getRepository('AppBundle:Questionnaire')
            ->findQuestionsByQuestionnaire($id);

        $termdata = $this ->getDoctrine()
            ->getRepository('AppBundle:TermData')
            ->findTermsByQuestionnaire($id);


        return $this->render('questionnaire/show.html.twig',
            array(
                'result' => $result,
                'questions'=>$question,
                'termdata'=>$termdata
            ));
    }

    /**
     * @Route("/editar/{id}", name="edit_questionnaire")
     */
    public function editar($id, Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneQuestionnaireById($id);

         if(!$result)
        {
            return $this->render('error/404.html.twig');
        }
        $form = $this->createForm(QuestionnaireType::class, $result);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $result->setName($result->getName());

            $questionnaire = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($questionnaire);
            $em->flush();


            return $this->redirectToRoute('show_by_DetailsQuestionnaire', array('id' => $id));
        }

        return $this->render('questionnaire/edit.html.twig', array(
            'form' => $form->createView(),
            'id' => $id
        ));
    }

    /**
     * @Route("/borrar/{id}/", name="delete_questionnaire")
     */
    public function delete($id, Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneQuestionnaireById($id);

        if(!$result)
        {
            return $this->render('error/404.html.twig');
        }
        $result->setStatus(Questionnaire::STATUS_INACTIVATE);

        $form = $this->createFormBuilder($result)
            ->add('delete', SubmitType::class, array('label' => 'Si'))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $ques = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($ques);
            $em->flush();

            return $this->redirectToRoute('show_by_DetailsQuestionnaire', array('id' => $id));
        }
        return $this->render('questionnaire/delete.html.twig', array(
            'form' => $form->createView(),
            'result' => $result
        ));
    }

    /**
     * @Route("/crear/", name="create_questionnaire")
     */
    public function crearQuestionnaire(Request $request)
    {
        $usr= $this->get('security.token_storage')->getToken()->getUser();

        if(!$usr)
        {
            return $this->render('error/404.html.twig');
        }
        $cuestionario = new Questionnaire();
        $form = $this->createForm(QuestionnaireType::class, $cuestionario);
        $form->handleRequest($request);
        $pre = $this->getDoctrine()
            ->getRepository('AppBundle:Question')
            ->findAll();
        if(!$pre)
        {
            return $this->render('error/404.html.twig');
        }
        if($form->isSubmitted())
        {
            $c = $request->request->get('questionnaire');

            $cuestionario->saveQuestionnaire(
                $c['name'],
                $c['description']
            );
            if ( ! array_key_exists('question', $c))
            {
                return $this->render('questionnaire/create.html.twig', array(
                    'form' => $form->createView(),
                    'pre' => $pre
                ));
            }
            $preguntas = $this->getDoctrine()
                ->getRepository('AppBundle:Question')
                ->findQuestionsByIds($c['question']);

            $cuestionario->setUser($usr);
            $cuestionario->addQuestions($preguntas);
            $cuestionario->setStatus(Questionnaire::STATUS_ACTIVATE);

            $cuestionario = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($cuestionario);
            $em->flush();

            return new JsonResponse('success');
        }
        return $this->render('questionnaire/create.html.twig', array(
            'form' => $form->createView(),
            'pre' => $pre
        ));

    }
}