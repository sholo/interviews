<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 24/08/2016
 * Time: 09:33 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserInterviewedType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/usuario")
 */
class UserInterviewedController extends Controller
{

    const REPOSITORY = 'AppBundle:User';

    /**
     * @Route("/crear/", name="create_user")
     */
    public function createUser(Request $request)
    {
        $user = new User();

        $form = $this->createForm(UserInterviewedType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            // Default data
            $user->setPassword($user->getEmail());
            $user->setUsername($user->getEmail());

            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('create_user_photo', array('user_id' => $user->getId()));

        }

        return $this->render('user/create.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/editar/{id}/", name="edit_user")
     */
    public function editUser($id, Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneUserById($id);

        if (!$result)
        {
            return $this->render('error/404.html.twig');
        }

        $form = $this->createForm(UserInterviewedType::class, $result);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            if($user->arrayToStringRoles($user->getRoles()) == "PROSPECT")
            {
                return $this->redirectToRoute('edit_user_photo', array('user_id' => $id));
            }
            else
            {
                return $this->redirectToRoute('show_user', array('id' => $id));
            }

        }

        return $this->render('user/edit.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/", name="show_all_users")
     */
    public function showAllUsers()
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findAllUsers();

        return $this->render('user/show_all.html.twig', [
            'result' => $result
        ]);

    }

    /**
     * @Route("/ver/{id}/", name="show_user")
     */
    public function showUser($id)
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findUserWithInfo($id);

        if ($result['user'] == null)
        {
            return $this->render('error/404.html.twig');
        }

        return $this->render('user/show.html.twig', [
            'result' => $result
        ]);
    }

    /**
     * @Route("/borrar/{id}/", name="delete_user")
     */
    public function deleteUser($id, Request $request)
    {

        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneUserById($id);

        if (!$result)
        {
            return $this->render('error/404.html.twig');
        }

        $result->setStatus(User::STATUS_INACTIVATE);

        $form = $this->createFormBuilder($result)
            ->add('delete', SubmitType::class, array('label' => 'Si'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            // $form->getData() holds the submitted values
            // but, the original `$result` variable has also been updated
            $user = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if User is a Doctrine entity, save it!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('show_user', array('id' => $id));
        }

        return $this->render('user/delete.html.twig', array(
            'form' => $form->createView(),
            'result' => $result
        ));

    }

}