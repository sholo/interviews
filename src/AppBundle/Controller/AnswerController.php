<?php
/**
 * Created by PhpStorm.
 * User: Jose Luis
 * Date: 07/09/2016
 * Time: 10:12 AM
 */

namespace AppBundle\Controller;

use AppBundle\Form\AnswerTypeTwo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/respuesta")
 */

class AnswerController extends Controller
{
    const REPOSITORY = 'AppBundle:Answer';

    /**
     * @Route("/editar/{id}/", name="edit_answer")
     */
    public function editAnswer($id, Request $request)
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneAnswerById($id);

        if (!$result)
        {
            return $this->render('error/404.html.twig');
        }

        $right = $result->getRight() == 1 ? TRUE : FALSE;
        $result->setRight($right);

        $form = $this->createForm(AnswerTypeTwo::class, $result);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $answer_right = $result->getRight() == TRUE? 1 : 0;
            $result->setRight($answer_right);

            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('show_all_questions');
        }

        return $this->render('answer/edit.html.twig', array(
            'form' => $form->createView(),
        ));

    }

}