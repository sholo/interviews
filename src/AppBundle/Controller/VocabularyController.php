<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 10/08/2016
 * Time: 11:44 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Vocabulary;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\VocabularyType;


/**
 * @Route("/catalogo")
 */
class VocabularyController extends Controller
{

    const REPOSITORY = 'AppBundle:Vocabulary';

    public function getVocabulariesVisiblesAction()
    {
        $vocabularies_visibles = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findVocabulariesVisibles();

        return $this->render('vocabulary/vocabularies_visibles.html.twig', array(
            'vocabularies_visibles' => $vocabularies_visibles,
        ));
    }

    /**
     * @Route("/", name="show_all_vocabularies")
     */
    public function showAllVocabularies()
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findAllVocabularies();

        return $this->render( 'vocabulary/show_all.html.twig', [
            'result' => $result
        ]);
    }

    /**
     * @Route("/ver/{id}/", name="show_vocabulary")
     */
    public function showVocabulary($id)
    {
        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneVocabularyById($id);

        if(!$result)
        {
            return $this->render('error/404.html.twig');
        }

        return $this->render( 'vocabulary/show.html.twig', array('result' => $result));
    }

    /**
     * @Route("/editar/{id}/", name="edit_vocabulary")
     */
    public function editVocabulary($id, Request $request)
    {

        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneVocabularyById($id);

        if (!$result)
        {
            return $this->render('error/404.html.twig');
        }

        $form = $this->createForm(VocabularyType::class, $result);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            //Set the Machine Name of the Vocabulary, taking the name and applying format
            $result->setMachineName($result->getName());

            $vocabulary = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($vocabulary);
            $em->flush();

            return $this->redirectToRoute('show_all_vocabularies');
        }

        return $this->render('vocabulary/edit.html.twig', array(
            'form' => $form->createView(),
            'id' => $id
        ));

    }

    /**
     * @Route("/crear/", name="create_vocabulary")
     */
    public function createVocabulary(Request $request)
    {
        $vocabulary = new Vocabulary();

        $form = $this->createForm(VocabularyType::class, $vocabulary);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            //Set the Machine Name of the Vocabulary, taking the name and applying format
            $vocabulary->setMachineName($vocabulary->getName());

            $vocabulary = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($vocabulary);
            $em->flush();

            return $this->redirectToRoute('show_all_vocabularies');
        }

        return $this->render('vocabulary/create.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/borrar/{id}/", name="delete_vocabulary")
     */
    public function deleteVocabulary($id, Request $request)
    {

        $result = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOneVocabularyById($id);

        if (!$result)
        {
            return $this->render('error/404.html.twig');
        }

        $result->setStatus(Vocabulary::STATUS_INACTIVATE);

        $form = $this->createFormBuilder($result)
            ->add('delete', SubmitType::class, array('label' => 'Si'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $vocabulary = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($vocabulary);
            $em->flush();

            return $this->redirectToRoute('show_vocabulary', array('id' => $id));
        }

        return $this->render('vocabulary/delete.html.twig', array(
            'form' => $form->createView(),
            'result' => $result
        ));

    }

}