<?php
/**
 * Created by PhpStorm.
 * User: eselejuanito
 * Date: 01/09/2016
 * Time: 10:39 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Form\PhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/usuario/foto")
 */
class PhotoController extends Controller
{
    const REPOSITORY = 'AppBundle:Photo';

    /**
     * @Route("/crear/{user_id}/", name="create_user_photo")
     */
    public function createPhoto($user_id, Request $request)
    {

        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneUserById($user_id);

        // Checking if the user ID exists, if not, we need to redirect
        if (!$user)
        {
            return $this->render('error/404.html.twig');
        }

        // Checking if the user doesn't has photo, if it's right, we need to redirect to show all users
        $photo = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOnePhotoByUser($user_id);

        if ($photo)
        {
            return $this->redirectToRoute('edit_user_photo', array('user_id' => $user_id));
        }

        $photo = new Photo();

        $form = $this->createForm(PhotoType::class, $photo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $photo->setUser($user);
            $file_name = $photo->photoManager($photo->getName(), $this->getParameter('images_directory'));

            // Update the 'name' and 'path' property to store the Photo file name
            $photo->setName($file_name);
            $photo->setPath($this->getParameter('relative_path_images') . $file_name);

            $photo = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();

            if($user->arrayToStringRoles($user->getRoles()) == "PROSPECT")
            {
                return $this->redirectToRoute('create_user_information', array('user_id' => $user_id));
            }
            else
            {
                return $this->redirectToRoute('show_all_users');
            }

        }

        return $this->render('photo/create.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/editar/{user_id}/", name="edit_user_photo")
     */
    public function editPhoto($user_id, Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneUserById($user_id);

        if (!$user)
        {
            return $this->render('error/404.html.twig');
        }

        // Checking if the user has photo, if it's right
        $photo = $this->getDoctrine()
            ->getRepository($this::REPOSITORY)
            ->findOnePhotoByUser($user_id);

        if (!$photo)
        {
            return $this->render('error/404.html.twig');
        }

        $same_name = explode('.', $photo->getName());
        $photo->setName(null);

        $form = $this->createForm(PhotoType::class, $photo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            $new_photo = $form->getData();

            $file_name = $photo->photoManager($new_photo->getName(), $this->getParameter('images_directory'), $same_name);

            $new_photo->setUser($user);
            $new_photo->setName($file_name);
            $new_photo->setPath($this->getParameter('relative_path_images') . $file_name);

            $em = $this->getDoctrine()->getManager();
            $em->persist($new_photo);
            $em->flush();

            if($user->arrayToStringRoles($user->getRoles()) == "PROSPECT")
            {
                return $this->redirectToRoute('edit_user_information', array('user_id' => $user_id));
            }
            else
            {
                return $this->redirectToRoute('show_all_users');
            }

        }

        return $this->render('photo/create.html.twig', array(
            'form' => $form->createView(),
        ));

    }
}